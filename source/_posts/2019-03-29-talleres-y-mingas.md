---
layout: blog
title: Talleres y Mingas
date: 2019-03-17T18:34:10.771Z
cover: /images/uploads/flayer-taller-camping-pura-isla-otro.jpg
---
Comunícate con nosotros para conocer las próximas fechas y para solicitar realizar un taller en tu barrio u organización.

Los talleres son abiertos y enfocados en 3 aristas:

* Teoría y práctica de construcción de domos geodésicos. 
* Modelo de Gobernanza, trabajo en equipo y sistemas complejos mediante la metodología gamificada.
* Herramientas financieras para un mundo nuevo. Uso de criptomonedas para realizar intercambios.

**El Próximo se** **realizará el 19/10/2019 **en el Camping "Pura Isla" (abajo esta la ubicación). Es abierto a toda la familia y en caso de asistir con niñes nos organizaremos para el cuidado.

**Horario de 9 hs a 21 hs. **Incluye almuerzo y fogón/cena de cierre (es importante la puntualidad)

**Completa tus datos** **(**[**AQUI**](https://forms.gle/C3F34rsCyPNqMTXDA)**)** y reserva un lugar en los talleres (**cupo máximo 30 personas**).

**Todos los talleres son GRATIS** (se aceptan colaboraciones a la Gorra o aportando a la [campaña de financiamiento colectivo](https://kolabora.komun.org/es/c/hormiguero-geodesico/) )

Los asistentes recibirán un **juego para armar**, una **giftcard con FAIRCOINS** (es conveniente [descargar la ](https://fair-coin.org/en/faircoin-wallets)APP antes de asistir al taller) y el link de **ingreso al**[** Grupo de Domicultores**](https://telegra.ph/Comunidad-de-Domicultores-08-23) donde encontraran mucha bibliografia y una comunidad de constructores.

**Deberán llevar** (en lo posible) algunas herramientas como Martillo, Pinza, Teneza, Sierra o Serrucho. 

**Aquí** 

<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2076.10487613136!2d-60.82586321547129!3d-31.76557992582925!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x95b5a45712fcab57%3A0x1bad3d3d778bebd2!2sPura%20isla%20camping!5e0!3m2!1ses!2sar!4v1571058008978!5m2!1ses!2sar" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
