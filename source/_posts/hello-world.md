---
title: Sumate al Hormiguero
date: 2019-03-26T19:08:28.205Z
cover: ''
---
Colabora con [Un proyecto de interés social](https://docs.google.com/forms/d/e/1FAIpQLSfpSKZTDirTI8c85lWVo47mkD9pGXds1VretLAlW5ep_S_XkQ/viewform) .

Mira Nuestro álbum de fotos [AQUI](https://photos.google.com/share/AF1QipPUfJ1dcmpUeC0kkgIIaP5docyVW1Y9kMlWDUzhcE9xMpprZQsGJgyUNUw5ieNIIw?key=dFgtZ1h5WEJkTzE3OTVNUnlYYmZITk1ERmY5OGhR) donde estan las imágenes de los diferentes talleres y Mingas que realizamos.

Podemos aprender del carácter descentralizado del trabajo colectivo de las hormigas, que las dota de una resiliencia ante eventos destructivos. Se auto-organizan. 

Asi es que iniciamos este proyecto, conocelo y participá. Vas a aprender sobre estructuras geodésicas, trabajo en equipo e intercambios con criptomonedas. 

![](/images/uploads/1.jpg)

Recibimos donaciones de Faircoins

![null](/images/uploads/qr-donaciones.png)

![](/images/uploads/2.jpg)
