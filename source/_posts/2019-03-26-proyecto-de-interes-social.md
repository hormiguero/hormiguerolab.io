---
layout: blog
title: Proyecto de Interes Social
date: 2019-02-03T19:49:12.696Z
cover: /images/uploads/imagen-telegraph.jpg
---
Construimos un centro experimental de tecnologías apropiadas que lo llamamos Hormiguero porque simulamos el trabajo de las hormigas que cargan y transportan organizadamente alimentos y materiales para su hogar; y es Geodésica la tecnología apropiada que aplicamos y aprendemos para lograr el vinculo entre las personas de la comunidad.

Ubicacion: https://goo.gl/maps/eas6BgQMCKR2

Álbum de Fotos: https://photos.app.goo.gl/2vP8xkVbNZY4r5nV9

Formá parte de la red de colaboradores. MAS INFO https://t.me/redhormiguero

![](/images/uploads/imagen-hormiguero-a4.png)
