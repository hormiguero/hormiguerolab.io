---
layout: blog
title: Sobre Nosotros
date: 2019-03-16T18:14:43.244Z
cover: /images/uploads/post4.png
---
Podes sumarte y cooperar con nosotros de diversas maneras. [Seguí este enlace](https://forms.gle/C3F34rsCyPNqMTXDA) y tambien puedes apoyarnos en la campaña de [financiamiento colectivo](https://kolabora.komun.org/es/c/hormiguero-geodesico/)

Premio INNOVACIÓN SOCIAL del concurso del municipio de Santa fe Argentina https://www.lt10.com.ar/noticia/249990--con-santa-fe-emprende-se-ve-el-enorme-potencial-de-un-sector-pujante-de-la-ciudad
