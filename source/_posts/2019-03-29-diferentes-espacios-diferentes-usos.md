---
layout: blog
title: 'Diferentes Espacios, diferentes usos'
date: 2019-03-18T18:23:00.000Z
cover: /images/uploads/post3.png
---
Diferentes materiales y técnicas. Un lugar para experimentar y desarrollar la creatividad.

[Aqui](https://photos.app.goo.gl/iWIdZ6XtuwZ1EmfN2) podes ver una selección de imágenes de Domos Geodésicos para alimentar tu creatividad.

Conocé nuestra propuesta didáctica de [ALQUIMETRICOS](http://alquimetricos.com/) kits de juguetes, kits para fabricación de juguetes, guías didácticas para educadorxs y actividades para hacer en la escuela, al aire libre, en familia.

![null](/images/uploads/alquimetrico.png)
