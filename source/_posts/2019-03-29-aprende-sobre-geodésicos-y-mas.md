---
layout: blog
title: Aprende sobre Geodésicos y mas..
date: 2019-03-19T18:39:00.000Z
cover: /images/uploads/post1-2-.png
---
* Domos geodésicos: Richard Buckminster Fuller (1895-1983), fue un cientifico del diseño, que se hizo famoso por el estudio que hizo sobre los domos geodésicos. Intentó crear un espacio para habitar más sostenible y más humano, que además pudiese ser fácilmente construido por cualquier persona. Partiendo de la figura geométrica más sencilla y más resistente que existe, el triángulo, y basándose en los sólidos platónicos, desarrolló una de las estructuras más fuertes que se conocen. Su forma curva es capaz de soportar terremotos de gran magnitud, además de vientos huracanados o la acumulación de nieve. Son estructuras que se autosustentan, por lo que no necesitan columnas, y son relativamente fáciles de construir y transportar.



* Modelo de Gobernanza y SINTEGRACION en equipo: es una metodología innovadora que incorpora el uso de poliedros 
  regulares para diseñar reuniones y trabajos en equipo, en torno a un tema de interés común, buscando desarrollar habilidades cooperativas, proyectos e ideas.

"Funciona como una máquina de compresión de tiempo máxima información compartida en un tiempo mínimo"

* [FAIRCOIN](https://fair-coin.org/es), es una criptomoneda basada en Blockchain (Cadena de Bloques) que se usa en un movimiento cooperativo internacional y abierto. La comunidad de usuarios comparten los principios y la gran variedad de herramientas financieras relacionadas. NOSOTROS USAMOS ESTA MONEDA y difundimos su uso explicando y compartiendo sus beneficios
