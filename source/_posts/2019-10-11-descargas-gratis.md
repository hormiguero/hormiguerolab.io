---
layout: blog
title: Fanzines y mas
date: 2019-03-19T18:50:00.000Z
cover: /images/uploads/descarga.png
---
Te facilitamos información y maquetas para comenzar a introducirte en este mundo geodésico

Nuestro principal objetivo es compartir el conocimiento por lo que en este apartado podrán descargar libremente todos los Fanzines y algunos instructivos más.

Elegimos la modalidad Fanzine para que sea económico y de fácil reproducción permitiendo que la información sea accesible a todas. Puede ser impreso en hoja A4 blanco y negro, aunque, si la impresión es a color y en hoja A3 se logra una lectura más cómoda. 

Son 12 publicaciones mas una décimo tercera integradora. El número definido hace referencia a la metodología que se usó para su elaboración donde los 12 temas elegidos se relacionan con los 12 vértices de un Icosaedro. Esto permitió que la estructura total esté compuesta por varios temas que atraviesan la cultura geodésica. 

Cada Fanzine contiene un LADO A con disparadores de información y un LADO B con una actividad práctica/lúdica. 

El décimo tercer Fanzine es una síntesis del proceso, un ofrecimiento de otra manera de concebir el tiempo. 

Muchas veces la excusa para postergar el cumplimiento de sueños u objetivos es la falta de tiempo, por lo tanto, como el tiempo actualmente es una convención impuesta podemos simplemente reemplazar esa convención por otra que se adapte mas a nuestra naturaleza.

Así que ahora no tienen excusa para descargarse todos los fanzines, comenzar a jugar con ellos y compartirlos.    

[Fanzine HG #01](https://gitlab.com/diretamozo/hormiguero-geodesico/-/wikis/uploads/110492f3084d53b12728f02e1a4119ff/fanzineHG01.pdf) contiene un Mapa Dymaxion (Icosaedro) para armar. Tambien pueden [descargar otra variante](https://aldeadomo.cl/wp-content/uploads/2019/08/icosaedro_magico_aldeadomo.pdf) para colorear de nuestros amigos de [Aldea Domo](https://aldeadomo.cl/2019/08/17/155/)

[Fanzine HG #02](https://gitlab.com/diretamozo/hormiguero-geodesico/-/wikis/uploads/8986e948d96ee1a01944f20cc47b216c/fanzineHG02.pdf) contiene una plantilla para armar un icosaedro con conectores, para descargar una variante en colores ---> [Juego Plantilla Icosaedro de colores con conectores ](https://www.canva.com/design/DADZd4fqSsA/dim3i722ZhTNTxUc5gVzJw/view)

[Fanzine HG #03](https://gitlab.com/diretamozo/hormiguero-geodesico/-/wikis/uploads/e3641bf76bb4857a64ebe05610c038a7/fanzineHG03.pdf) contiene molde para armar un Alquimétrico

[Fanzine HG #04](https://gitlab.com/diretamozo/hormiguero-geodesico/-/wikis/uploads/02e6e79ed1754598a08516bd9a4fa858/fanzineHG04.pdf) contiene instrucciones para armar un domo frecuencia 2 con botellas PET

[Fanzine HG #05](https://gitlab.com/diretamozo/hormiguero-geodesico/-/wikis/uploads/d74bb9ac0082c3c699bfa9127cd30b3d/fanzineHG05.pdf) contiene una plantilla para armar medio icosaedro con diseños OpArt

[Fanzine HG #06](https://gitlab.com/diretamozo/hormiguero-geodesico/-/wikis/uploads/eac303386bef58ae5dc3210237f9570f/fanzineHG06.pdf) contiene instrucciones para armar un domo económico frecuencia 2 con conectores

[Fanzine HG #07](https://gitlab.com/diretamozo/hormiguero-geodesico/-/wikis/uploads/23179202bc4e0899a12db8d3065fb31f/fanzineHG07.pdf) contiene instrucciones para armar un domo frecuencia 3 con conectores

[Fanzine HG #08](https://gitlab.com/diretamozo/hormiguero-geodesico/-/wikis/uploads/657b51196248f3b6dbbd527bf4924ee4/fanzineHG08.pdf) contiene plantillas e instrucciones para armar un domo sin conectores

[Fanzine HG# 09](https://gitlab.com/diretamozo/hormiguero-geodesico/-/wikis/uploads/876142e2e51fad57b4c8023831675fbf/fanzineHG09.pdf) contiene una plantilla para hacer una maqueta Zome

[Fanzine HG #10](https://gitlab.com/diretamozo/hormiguero-geodesico/-/wikis/uploads/d68b6c9f294035a675434ca548614415/fanzineHG10.pdf) contiene instrucciones para armar una Lámpara Rotegrity

[Fanzine HG #11](https://gitlab.com/diretamozo/hormiguero-geodesico/-/wikis/uploads/003f01664082965523e022c3742201e3/fanzineHG11.pdf) contiene descripción de los cimientos del Proyecto para tener en cuenta y replicarlo.

[Fanzine HG #12](https://gitlab.com/diretamozo/hormiguero-geodesico/-/wikis/uploads/c3ee3c985d3cb485ceca5fbbae00a7a2/fanzineHG12.pdf) contiene 2 modelos de cubiertas para el domo del Fanzine #06

[Fanzine HG #13](https://gitlab.com/diretamozo/hormiguero-geodesico/-/wikis/uploads/a7c6724ee6bf01276e9b31dd29145c51/fanzineHG13.pdf) contiene un icosaedro con un calendario lunar y su equivalencia con el calendario gregoriano.

Muchas gracias a todas las que participaron directa o indirectamente en la elaboración, aún es probable que alguna información merezca revisión por lo cual se agradecen las opiniones [aqui](https://hormiguero.gitlab.io/2019/03/26/hello-world/) o en el grupo de [telegram](https://t.me/hormiguerogeodesico). Pronto agregaremos el encuadernado con los indices, agradecimientos y propuestas de talleres.
