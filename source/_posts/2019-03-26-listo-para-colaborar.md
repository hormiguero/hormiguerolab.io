---
layout: blog
title: Listo para Colaborar ????
date: 2019-02-04T20:00:00.000Z
cover: /images/uploads/niño-contento.jpg
---
Te invitamos a 

1. Participar de los talleres/Mingas; 
2. Formar parte de la red de colaboradores  
3. Participar de la campaña de [financiamiento colectivo](https://kolabora.komun.org/es/c/hormiguero-geodesico/)
4. Donar Faircoin: fahMVcgp6mAbAYzUyvEVtA385SLJrQNVPg 

[**Contactate**](https://docs.google.com/forms/d/e/1FAIpQLSfpSKZTDirTI8c85lWVo47mkD9pGXds1VretLAlW5ep_S_XkQ/viewform)** asi logramos replicar la propuesta a continuación te mostramos nuestra hoja de ruta, ya estamos comenzando la ETAPA II ¡¡!!**

![null](/images/uploads/faricoin.jpg)
