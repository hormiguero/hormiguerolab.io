---
layout: blog
title: Campaña de financiamiento colectivo en Kolabora
date: 2019-03-15T21:01:00.000Z
cover: /images/uploads/hormiguero.png
---
Buscá nuestra Campaña de Financiamiento [Aqui](https://kolabora.komun.org/es/c/hormiguero-geodesico/) y contribuye a crear muchos domos geodesicos para diferentes fines. Además obtendrás diferentes recompensas en función del aporte realizado.
