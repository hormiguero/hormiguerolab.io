---
layout: blog
title: En los Medios Locales
date: 2019-03-16T20:31:00.000Z
cover: /images/uploads/medios.jpg
---
**Diario El Litora**l [**"Un domo inédito y una moneda virtual para cambiarle la vida a todo un barrio "**](https://www.ellitoral.com/index.php/id_um/191299-un-domo-inedito-y-una-moneda-virtual-para-cambiarle-la-vida-a-todo-un-barrio-se-construye-en-barrio-los-troncos-area-metropolitana.html) El proyecto se llama “Hormiguero Geodésico”. La estructura geométrica del domo -con forma de cúpula- se hace con elementos donados o reciclados. El espacio busca fomentar los lazos sociales, el trabajo comunitario, la apropiación de saberes técnicos y un microsistema económico para los vecinos con una moneda virtual.

![null](/images/uploads/diario-litoral.png)

**Programa matutino Arriba Santa FE** - [ver aqui](https://youtu.be/w_QMBphTOmM) - 

![null](/images/uploads/arribasanta-fe.jpg)

**Micro de Economia Social en ATP - **[**ver aqui**](https://youtu.be/tIf4EU1bzlI)**\-**

"Encontrar en el mutualismo y en el cooperativismo las herramientas para resolver todos los problemas y necesidades sociales que tiene la comunidad en general" Diego Retamozo - Coordinador del proyecto "Domo Geodesico" 

![null](/images/uploads/en-atp.jpg)

Lt10 Comunicado de los Proyectos Premiados - Hormiguero Geodésico GANADOR en Innovación Social 

https://www.lt10.com.ar/noticia/249990--con-santa-fe-emprende-se-ve-el-enorme-potencial-de-un-sector-pujante-de-la-ciudad
