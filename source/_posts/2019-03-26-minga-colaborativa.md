---
layout: blog
title: Minga te voy a dar
date: 2018-12-01T19:40:25.319Z
cover: /images/uploads/barro.png
---
Revivimos la antigua tradición Quechua del trabajo comunitario. Aprenderemos sobre construcción natural, estructuras geodésicas. Mientras compartiremos mates, almuerzo a la canasta y música. 

![Para aprender, construir y divertirse](/images/uploads/minga-2.jpg)
