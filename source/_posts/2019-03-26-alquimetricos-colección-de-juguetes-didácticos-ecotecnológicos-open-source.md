---
layout: blog
title: Alquimetricos. Colección de juguetes didácticos ecotecnológicos Open Source
date: 2018-11-03T19:18:07.929Z
cover: /images/uploads/flyeralquimetros.jpg
---
Taller didáctico de construcción de un Icosaedro con materiales reciclados. 

Alquimétricos es una colección de juguetes didácticos de código abierto (open source). Son bloques para construir estructuras y aprender, a través del juego, sobre geometría, matemáticas, arquitectura, mecánica, física, química y mucho más. La iniciativa está enfocada en el diseño y producción de juguetes didácticos DIY (hazlo tú mismo) y DIT (hagámoslo juntxs) producidos utilizando procesos analógicos y digitales, materiales reciclados y alternativos. Diseñados para jugar, crear, aprender y compartir.

Mas información en http://alquimetricos.com/. Para replicar estas experiencias educativas escribidnos
