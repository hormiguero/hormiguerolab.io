---
layout: blog
title: 'Faircoin: Una Moneda Justa '
date: 2019-03-13T20:44:58.117Z
cover: /images/uploads/aceptamos-faircoin.png
---
Porque creemos que otra economía es posible diseñamos un esquema de financiamiento y transferencia de conocimiento usando una criptomoneda. 

Podes conocer mas y colaborar donando Faircoin. Al adquirir un Bono contribucion te entregamos a cambio criptomonedas para que puedas experimentar el uso de la misma en los comercios adheridos en Santa Fe. Consulta [AQUI](https://map.komun.org/?l=es&lat=-31.61268&lon=-60.66633&zoom=13)

Tambien podes ser parte de la red de colaboradores que recibirán Gratis criptomonedas con el objetivo de que difundan la propuesta y participen de un taller de toma de decisiones. [SUMATE](https://t.me/redhormiguero)  

![](/images/uploads/qr-formulario-campaña.png)

![](/images/uploads/1.png)

![](/images/uploads/2.png)
